


;; It takes Org to tangle
(setq tangled-name (concat user-emacs-directory "tangled.el"))

(setq minimacs-configuration "~/.minimacs/init.org")
(defun minimacs-find-configuration () (interactive)
  (find-file minimacs-configuration))

(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                 (not (gnutls-available-p))))
    (proto (if no-ssl "http" "https")))
    ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
    (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
    ;;(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
    (when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
(add-to-list 'package-archives '("gnu" . (concat proto "://elpa.gnu.org/packages/")))))
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))


(use-package org)

(defun tangle-init()
  "If the current buffer is an org-file in the ~/.minimacs/ root the code-blocks are tangled, and the tangled file is compiled."
  (let
      ((minimacs_init (file-truename minimacs-configuration))
       (buffer_file (file-truename buffer-file-name)))
    (message "saving buffer")
    (message minimacs_init)
    (message buffer_file)
    (when (equal minimacs_init buffer_file)
      ;; Avoid running hooks when tangling.
      (let ((prog-mode-hook nil))
	(message "Rewrite config...")
	(org-babel-tangle-file minimacs-configuration tangled-name)
	(byte-compile-file tangled-name)))))
(add-hook 'after-save-hook 'tangle-init)

(unless (file-exists-p tangled-name)
  (progn
    (message "could not find a tangled file, trying to make one")
    (unless (file-exists-p "~/.minimacs/init.org")
      (make-directory "~/.minimacs")
      (copy-file (concat user-emacs-directory "template.org") "~/.minimacs/init.org"))
    ;; tangle file to replace this one
    (org-babel-tangle-file "~/.minimacs/init.org" tangled-name)
    ))

;; load the tangled file
(load-file (concat user-emacs-directory tangled-name))



(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-agenda-files (quote ("~/dotfiles/minimacs/.minimacs/init.org")))
 '(package-selected-packages
   (quote
    (org-mode evil-magit magit hydra which-key ace-window counsel general evil use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
