#!/bin/sh
[ -d ~/.emacs.d ] || mkdir ~/.emacs.d
[ -e ~/.emacs.d/init.el ] || ln -s $(pwd)/init.el ~/.emacs.d/init.el
[ -e ~/.emacs.d/template.org ] || ln -s $(pwd)/template.org ~/.emacs.d/template.org

